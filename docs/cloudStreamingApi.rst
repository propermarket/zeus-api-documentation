============
Cloud Streaming Api
============

.. container:: document

   .. container:: documentwrapper

      .. container:: bodywrapper

         .. container:: body

            .. rubric:: API End Points
               :name: index

            .. container:: genindex-jumpbox

               .. rubric:: Save cloud Configuration
                  :name: save-cloud-configuration

               It is to save your cloud credentials to server so as
               authenticate on cloud resources creation and deletion.
               .. rubric:: **EndPoint :
                  https://api.dev.zeusrtc.com/api/v1/provision/CloudConfiguration/save**
                  :name: endpoint-httpsapi.dev.zeusrtc.comapiv1provisioncloudconfigurationsave

               .. rubric:: **Request** : POST
                  **Body** :{
                  type : “aws”
                  orgId : orgId,
                  configuration: {
                  }
                  BodyType : json
                  Response :credentials object
                  :name: request-post-body-type-aws-orgid-orgid-configuration-bodytype-json-response-credentials-object

               **type** : Cloud configuration type
               **orgId** : organisation Id, It is provided after you
               register user to ZuesRTC Platform.
               You can fetch it by decoding the JWT that you get on
               authentication.
               **Configuration** : object of the configuration required
               to authorise the third party cloud platform.
               **credential Object** : Db object of saved cloud
               credential
               .. rubric:: Create cloud resources
                  :name: create-cloud-resources

               It is to create all the requisite resources ready at
               cloud server end before starting the streaming
               .. rubric:: **EndPoint :
                  https://api.dev.zeusrtc.com/api/v1/provision/CloudResources/create**
                  :name: endpoint-httpsapi.dev.zeusrtc.comapiv1provisioncloudresourcescreate

               .. rubric:: **Request** : POST
                  **Body** :{
                  configurationId : configurationId
                  }
                  BodyType : json
                  Response :resource object
                  :name: request-post-body-configurationid-configurationid-bodytype-json-response-resource-object

               **configurationId** : Id of cloud configuration. Get it
               from save cloud configuration response.
               **resource Object** : Db object of saved resource
               .. rubric:: Get cloud resources
                  :name: get-cloud-resources

               It is to fetch the status of created cloud resources